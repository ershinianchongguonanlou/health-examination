#!/bin/sh
#打包
mvn clean package --settings ./deploy/settings.xml -Dmaven.test.skip=true
#构建镜像
docker build -f ./deploy/tomcatDockerfile -t health-java/service-provider --build-arg PACKAGE_PATH=health_service_provider/target/*.war .
docker build -f ./deploy/tomcatDockerfile -t health-java/web-manager --build-arg PACKAGE_PATH=health_backend/target/*.war .
docker build -f ./deploy/tomcatDockerfile -t health-java/web-mobile --build-arg PACKAGE_PATH=health_mobile/target/*.war .

#上传镜像

#删除镜像
# docker rmi --force $(docker images | grep saas-export-java | awk '{print $3}')
#执行k8s命令
