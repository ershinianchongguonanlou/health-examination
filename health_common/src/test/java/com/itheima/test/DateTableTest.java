package com.itheima.test;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @ProjectName: project-health
 * @Title: DateTableTest
 * @Description: TODO
 * @Author: HeKuo
 * @Date: 2023/8/139:36
 */
public class DateTableTest {

    @Test
    public void test1() {
        //先格式化
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM");
        //获取当前年月
        LocalDate now = LocalDate.now();
//        String end = now.format(formatter);
        //获取11月前年月 -11
        LocalDate localDate = now.minusMonths(11);
//        String start = localDate.format(formatter);
//        //死循环方式向里面放数据
//        String[] dateTable = new String[12];
//        dateTable[0] = start;
//        for (int i = 1; i < dateTable.length; i++) {
//            localDate = localDate.plusMonths(1);
//            dateTable[i] = localDate.format(formatter);
//        }
        ArrayList<LocalDate> dateTable = new ArrayList<>();
        dateTable.add(localDate);
        while (!localDate.equals(now)) {
            localDate = localDate.plusMonths(1);
            dateTable.add(localDate);
        }
        System.out.println("dateTable = " + dateTable);
        //[2022.09, 2022.10, 2022.11, 2022.12, 2023.01, 2023.02,
        // 2023.03, 2023.04, 2023.05, 2023.06, 2023.07, 2023.08]
        //System.out.println("Arrays.toString(dateTable) = " + Arrays.toString(dateTable));
    }
}
