package com.itheima.test;

import org.apache.commons.lang.text.StrTokenizer;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @ProjectName: project-health
 * @Title: ExcelText
 * @Description: TODO
 * @Author: HeKuo
 * @Date: 2023/8/169:11
 */
public class ExcelText {

    /**
     * 导出为Excel（无模板）
     *
     * @throws IOException
     */
    @Test
    public void test1() throws IOException {
        //1、获取数据源
        String name = "张三";
        Integer age = 18;
        String address = "北京市海滨区";
        //表头
        String title = "学生信息";
        String firstCell = "姓名";
        String secondCell = "年龄";
        String thirdCell = "地址";
        //2、导入依赖，POI
        FileOutputStream fos = new FileOutputStream(new File("D:/测试学生信息表.xlsx"));
        //3、创建对象，把数据源放进表格里面
        XSSFWorkbook excel = new XSSFWorkbook();
        createTableSetValue(name, age, address, title, firstCell, secondCell, thirdCell, excel);
        //8、导出Excel
        //输出流
        excel.write(fos);
        //9、刷新，关闭流
        fos.flush();
        fos.close();
        excel.close();
    }

    private void createTableSetValue(String name, Integer age, String address, String title, String firstCell, String secondCell, String thirdCell, XSSFWorkbook excel) {
        //4、获得一个sheet
        XSSFSheet sheet = excel.createSheet("学生信息表");
        //5、获得第一行,从0开始到
        XSSFRow row0 = sheet.createRow(0);
        //6、获得第1格
        XSSFCell cell0 = row0.createCell(0);
        //7、第一行，第一格，放值
        cell0.setCellValue(title);

        //第二行
        XSSFRow row1 = sheet.createRow(1);
        for (int i = 0; i < 3; i++) {
            XSSFCell cell = row1.createCell(i);
            if (i == 0) {
                cell.setCellValue(firstCell);
            } else if (i == 2) {
                cell.setCellValue(secondCell);
            } else {
                cell.setCellValue(thirdCell);
            }
        }

        //第三行
        XSSFRow row2 = sheet.createRow(2);
        for (int i = 0; i < 3; i++) {
            XSSFCell cell = row2.createCell(i);
            if (i == 0) {
                cell.setCellValue(name);
            } else if (i == 2) {
                cell.setCellValue(age);
            } else {
                cell.setCellValue(address);
            }
        }
    }

}
