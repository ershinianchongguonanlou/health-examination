package com.itheima.Bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: project-health
 * @Title: OrderCountBo
 * @Description: 预约到诊数据统计
 * @Author: HeKuo
 * @Date: 2023/8/1315:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderCountBo {
    private Integer todayOrderCount;
    private Integer thisWeekOrderCount;
    private Integer thisMonthOrderCount;

    private Integer todayComeHereCount;
    private Integer thisWeekComeHereCount;
    private Integer thisMonthComeHereCount;
}
