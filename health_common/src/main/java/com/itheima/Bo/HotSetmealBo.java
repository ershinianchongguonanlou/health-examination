package com.itheima.Bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: project-health
 * @Title: HotSetmealBo
 * @Description: 热门套餐--数据统计
 * @Author: HeKuo
 * @Date: 2023/8/1314:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HotSetmealBo {
    //套餐名称
    private String setmealName;
    //预约数量
    private Integer orderCount;
    //预约占比
    private Double orderProportion;
    //备注
    private String attention;

}
