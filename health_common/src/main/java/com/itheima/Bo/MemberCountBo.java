package com.itheima.Bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: project-health
 * @Title: MemberCountBo
 * @Description: 会员数据统计
 * @Author: HeKuo
 * @Date: 2023/8/1315:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MemberCountBo {
    private Integer todayInsertMember;
    private Integer totalInsertMember;
    private Integer thisWeekInsertMember;
    private Integer thisMonthInsertMember;
}
