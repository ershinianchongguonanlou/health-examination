package com.itheima.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@Slf4j
public class GlobalExceptionHandler implements HandlerExceptionResolver {

    /**
     * 通过实现借口HandlerExceptionResolver的resolveException方法，
     * 来完成日志的记录，而且还可以指定程序发生错误后跳转的web页面
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        log.error("", ex);
        new ModelAndView();
        return new ModelAndView((mode, vrequest, vresponse) -> {
            response.sendError(500, "服务器内部发生了错误！");
        }).addObject("exception", ex);
    }
}
