package com.itheima.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 封装查询条件
 */
@ApiModel("QueryPageBean-封装查询条件")
public class QueryPageBean implements Serializable {

    @ApiModelProperty(value = "页码")
    private Integer currentPage;//页码
    @ApiModelProperty(value = "每页记录数")
    private Integer pageSize;//每页记录数
    @ApiModelProperty(value = "查询条件")
    private String queryString;//查询条件
    public Integer getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
    public Integer getPageSize() {
        return pageSize;
    }
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    public String getQueryString() {
        return queryString;
    }
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
}
