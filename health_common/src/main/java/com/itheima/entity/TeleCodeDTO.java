package com.itheima.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 手机验证码相关
 */
@ApiModel("TeleCodeDTO-手机验证码相关")
@Data
public class TeleCodeDTO implements Serializable{
    @ApiModelProperty(value = "手机号")
    private String telephone;//手机号

    @ApiModelProperty(value = "验证码")
    private String validateCode;//验证码


}
