package com.itheima.Do;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: project-health
 * @Title: SetmealDo
 * @Description: TODO
 * @Author: HeKuo
 * @Date: 2023/8/1317:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SetmealDo {
    private Integer setmealId;
    private Integer setmealCount;
}
