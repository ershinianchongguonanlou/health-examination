package com.itheima.Vo;

import io.swagger.annotations.ApiModel;

/**
 * @ProjectName: project-health
 * @Title: MemberRegVo
 * @Description: 会员注册
 * @Author: HeKuo
 * @Date: 2023/8/1310:17
 */
@ApiModel("MemberRegVo-会员注册")
public class MemberRegVo {
    private String months;
    private Integer memberCount;

    @Override
    public String toString() {
        return "MemberRegVo{" +
                "months='" + months + '\'' +
                ", memberCount=" + memberCount +
                '}';
    }

    public MemberRegVo(String months, Integer memberCount) {
        this.months = months;
        this.memberCount = memberCount;
    }

    public MemberRegVo() {
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public Integer getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Integer memberCount) {
        this.memberCount = memberCount;
    }
}
