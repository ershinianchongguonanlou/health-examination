package com.itheima.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 会员
 */
@ApiModel("Member-会员")
public class Member implements Serializable{
    @ApiModelProperty(value = "主键")
    private Integer id;//主键
    @ApiModelProperty(value = "档案号")
    private String fileNumber;//档案号
    @ApiModelProperty(value = "姓名")
    private String name;//姓名
    @ApiModelProperty(value = "性别")
    private String sex;//性别
    @ApiModelProperty(value = "身份证号")
    private String idCard;//身份证号
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;//手机号
    @ApiModelProperty(value = "注册时间")
    private Date regTime;//注册时间
    @ApiModelProperty(value = "登录密码")
    private String password;//登录密码
    @ApiModelProperty(value = "邮箱")
    private String email;//邮箱
    @ApiModelProperty(value = "出生日期")
    private Date birthday;//出生日期
    @ApiModelProperty(value = "备注")
    private String remark;//备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getRegTime() {
        return regTime;
    }

    public void setRegTime(Date regtime) {
        this.regTime = regtime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
