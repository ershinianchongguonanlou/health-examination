package com.itheima.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 预约设置
 */
@ApiModel("OrderSetting-预约设置")
public class OrderSetting implements Serializable{
    @ApiModelProperty(value = "主键")
    private Integer id ;
    @ApiModelProperty(value = "预约设置日期")
    private Date orderDate;//预约设置日期
    @ApiModelProperty(value = "可预约人数")
    private int number;//可预约人数
    @ApiModelProperty(value = "已预约人数")
    private int reservations ;//已预约人数

    public OrderSetting() {
    }

    public OrderSetting(Date orderDate, int number) {
        this.orderDate = orderDate;
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getReservations() {
        return reservations;
    }

    public void setReservations(int reservations) {
        this.reservations = reservations;
    }
}
