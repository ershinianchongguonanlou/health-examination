package com.itheima.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户
 */
@ApiModel("User-用户")
public class User implements Serializable{
    @ApiModelProperty(value = "主键")
    private Integer id; // 主键
    @ApiModelProperty(value = "生日")
    private Date birthday; // 生日
    @ApiModelProperty(value = "性别")
    private String gender; // 性别
    @ApiModelProperty(value = "用户名，唯一")
    private String username; // 用户名，唯一
    @ApiModelProperty(value = "密码-MD5加密")
    private String password; // 密码
    @ApiModelProperty(value = "备注")
    private String remark; // 备注
    @ApiModelProperty(value = "状态")
    private String station; // 状态
    @ApiModelProperty(value = "联系电话")
    private String telephone; // 联系电话
    @ApiModelProperty(value = "对应角色集合")
    private Set<Role> roles = new HashSet<Role>(0);//对应角色集合

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
