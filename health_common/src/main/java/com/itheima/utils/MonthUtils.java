package com.itheima.utils;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

/**
 * @ProjectName: project-health
 * @Title: MonthUtils
 * @Description: LocalDate获取月份的第一天和最后一天
 * @Author: HeKuo
 * @Date: 2023/8/1310:42
 */
//LocalDate获取月份的第一天和最后一天
public class MonthUtils {
    //LocalDate获取一个月第一天
    public static LocalDate getFirstDayOfMonth(LocalDate date) {
        return date.with(TemporalAdjusters.firstDayOfMonth());
    }

    //LocalDate获取一个月最后一天
    public static LocalDate getLastDayOfMonth(LocalDate date) {
        return date.with(TemporalAdjusters.lastDayOfMonth());
    }
}
