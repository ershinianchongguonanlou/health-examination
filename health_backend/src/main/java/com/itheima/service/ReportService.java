package com.itheima.service;

import com.itheima.Bo.HotSetmealBo;
import com.itheima.Bo.MemberCountBo;
import com.itheima.Bo.OrderCountBo;
import net.sf.jasperreports.engine.JRException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ReportService {
    public Map<String, Object> getBusinessReport() throws Exception;

    //查询套餐预约占比数据统计
    Map<String, Double> getSetmealNamesAndSetmealCount();

    //获得当日日期
    String getLocalDateNow();

    //会员数据统计
    MemberCountBo getMemberCount();

    //预约到诊数据统计
    OrderCountBo getOrderCount();

    //热门套餐,封装
    List<HotSetmealBo> getHotSetmeal();

    void exportExcel(HttpServletRequest request, HttpServletResponse response) throws IOException;

    //下载为PDF
    void exportPDF(HttpServletRequest request, HttpServletResponse response) throws Exception;

    //热门套餐,封装为map集合<String，List>
    Map setmealReportTransformToMap(Map<String, Double> setmealNamesAndSetmealCount);

    //获取运营数据统计，封装为map
    Map businessReportDataTransformToMap(Map<String, Object> businessReport);
}
