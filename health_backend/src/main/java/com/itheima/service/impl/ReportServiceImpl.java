package com.itheima.service.impl;

import com.itheima.Do.SetmealDo;
import com.itheima.Bo.HotSetmealBo;
import com.itheima.Bo.MemberCountBo;
import com.itheima.Bo.OrderCountBo;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.SetmealDao;
import com.itheima.pojo.Setmeal;
import com.itheima.service.ReportService;
import com.itheima.utils.MonthUtils;
import net.sf.jasperreports.engine.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 报表统计服务
 */
@Service
@Transactional
public class ReportServiceImpl implements ReportService {
    @Autowired
    private MemberDao memberDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private SetmealDao setmealDao;

    //统计运营数据报表
    public Map<String, Object> getBusinessReport() throws Exception {

        Map<String, Object> result = new HashMap<>();
        String today = getLocalDateNow();

        MemberCountBo memberCount = getMemberCount();
        Integer todayNewMember = memberCount.getTodayInsertMember();
        Integer totalMember = memberCount.getTotalInsertMember();
        Integer thisWeekNewMember = memberCount.getThisWeekInsertMember();
        Integer thisMonthNewMember = memberCount.getThisMonthInsertMember();

        OrderCountBo orderCount = getOrderCount();
        Integer todayOrderNumber = orderCount.getTodayOrderCount();
        Integer thisWeekOrderNumber = orderCount.getThisWeekOrderCount();
        Integer thisMonthOrderNumber = orderCount.getThisMonthOrderCount();
        Integer todayVisitsNumber = orderCount.getTodayComeHereCount();
        Integer thisWeekVisitsNumber = orderCount.getThisWeekComeHereCount();
        Integer thisMonthVisitsNumber = orderCount.getThisWeekComeHereCount();

        List<HotSetmealBo> hotSetmeal = getHotSetmeal();

        result.put("reportDate", today);
        result.put("todayNewMember", todayNewMember);
        result.put("totalMember", totalMember);
        result.put("thisWeekNewMember", thisWeekNewMember);
        result.put("thisMonthNewMember", thisMonthNewMember);
        result.put("todayOrderNumber", todayOrderNumber);
        result.put("thisWeekOrderNumber", thisWeekOrderNumber);
        result.put("thisMonthOrderNumber", thisMonthOrderNumber);
        result.put("todayVisitsNumber", todayVisitsNumber);
        result.put("thisWeekVisitsNumber", thisWeekVisitsNumber);
        result.put("thisMonthVisitsNumber", thisMonthVisitsNumber);
        result.put("hotSetmeal", hotSetmeal);

        return result;
    }

    //查询套餐预约占比数据统计
    @Override
    public Map<String, Double> getSetmealNamesAndSetmealCount() {
        //拿到所有预约套餐的id
        List<Integer> setmealIds = orderDao.findAllSetmealId();
        //返回值
        Map<String, Double> map = new HashMap<>();

        for (Integer setmealId : setmealIds) {
            //根据id拿name
            Setmeal setmeal = setmealDao.findById(setmealId);
            String setmealName = setmeal.getName();
            //拿到总人数
            Integer totalCcount = orderDao.findTotalCount(null, null, null);
            //根据套餐ID，拿到预约人数
            Integer count = orderDao.findCountBySetmeal(setmealId);
            //除法
            BigDecimal total = BigDecimal.valueOf((double) totalCcount);
            BigDecimal singleCount = BigDecimal.valueOf((double) count);
            //拿到百分比，小数
            double setmealCount = singleCount.divide(total, 2, RoundingMode.HALF_UP).doubleValue();
            //存到map里面
            map.put(setmealName, setmealCount);
        }

        return map;
    }

    //获得当日日期
    @Override
    public String getLocalDateNow() {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String localDateNow = now.format(formatter);
        return localDateNow;
    }

    //会员数据统计
    @Override
    public MemberCountBo getMemberCount() {
        LocalDate now = LocalDate.now();
        //获得今日注册会员人数
        Integer todayInsertMember = memberDao.findCountByRegTime(now, now);
        Integer totalInsertMember = memberDao.findCountByRegTime(null, null);
        LocalDate start = now.minusDays(6);
        Integer thisWeekInsertMember = memberDao.findCountByRegTime(start, now);
        //拿到本月第一天
        LocalDate firstDayOfMonth = MonthUtils.getFirstDayOfMonth(now);
        //拿到本月最后一天
        LocalDate lastDayOfMonth = MonthUtils.getLastDayOfMonth(now);
        Integer thisMonthInsertMember = memberDao.findCountByRegTime(firstDayOfMonth, lastDayOfMonth);
        MemberCountBo memberCountBo = MemberCountBo.builder()
                .todayInsertMember(todayInsertMember)
                .totalInsertMember(totalInsertMember)
                .thisWeekInsertMember(thisWeekInsertMember)
                .thisMonthInsertMember(thisMonthInsertMember)
                .build();
        return memberCountBo;
    }

    //预约到诊数据统计
    @Override
    public OrderCountBo getOrderCount() {
        //预约
        LocalDate now = LocalDate.now();
        Integer todayOrderCount = orderDao.findTotalCount(now, now, null);
        LocalDate start = now.minusDays(6);
        Integer thisWeekOrderCount = orderDao.findTotalCount(start, now, null);
        //拿到本月第一天
        LocalDate firstDayOfMonth = MonthUtils.getFirstDayOfMonth(now);
        //拿到本月最后一天
        LocalDate lastDayOfMonth = MonthUtils.getLastDayOfMonth(now);
        Integer thisMonthOrderCount = orderDao.findTotalCount(firstDayOfMonth, lastDayOfMonth, null);
        //到诊
        Integer todayComeHereCount = orderDao.findTotalCount(null, now, "到诊");
        Integer thisWeekComeHereCount = orderDao.findTotalCount(start, now, "到诊");
        Integer thisMonthComeHereCount = orderDao.findTotalCount(firstDayOfMonth, lastDayOfMonth, "到诊");

        OrderCountBo orderCountBo = OrderCountBo.builder()
                .todayOrderCount(todayOrderCount)
                .thisWeekOrderCount(thisWeekOrderCount)
                .thisMonthOrderCount(thisMonthOrderCount)
                .todayComeHereCount(todayComeHereCount)
                .thisWeekComeHereCount(thisWeekComeHereCount)
                .thisMonthComeHereCount(thisMonthComeHereCount)
                .build();
        return orderCountBo;
    }

    //热门套餐,封装
    @Override
    public List<HotSetmealBo> getHotSetmeal() {
        //返回值
        List<HotSetmealBo> hotSetmealBoList = new ArrayList<>();
        //怎么才算热门套餐
        //预约最高的前四个

        //拿到总预约数量
        Integer totalCount = orderDao.findTotalCount(null, null, null);
        BigDecimal total = BigDecimal.valueOf((double) totalCount);

        List<SetmealDo> setmealDoList = orderDao.findHotSetmealId();
        //Map<Integer, Integer> hotSetmealMap = orderDao.findHotSetmealId();
        //我们发现setmealCount==null,没有值
        //遍历放值
        for (SetmealDo setmealDo : setmealDoList) {
            Integer setmealId = setmealDo.getSetmealId();
            //拿数量
            Integer orderCount = orderDao.findCountBySetmeal(setmealId);
            if (orderCount == null) {
                orderCount = 0;
            }
            BigDecimal singleCount = BigDecimal.valueOf((double) orderCount);
            double orderProportion = singleCount.divide(total, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
            //拿名字

            Setmeal setmeal = setmealDao.findById(setmealId);
            String setmealName = setmeal.getName();
            String attention = setmeal.getAttention();

            HotSetmealBo hotSetmealBo = HotSetmealBo.builder()
                    .setmealName(setmealName)
                    .orderCount(orderCount)
                    .orderProportion(orderProportion)
                    .attention(attention)
                    .build();
            hotSetmealBoList.add(hotSetmealBo);
        }
        return hotSetmealBoList;


   /*     Set<Map.Entry<Integer, Integer>> entries = hotSetmealMap.entrySet();
        for (Map.Entry<Integer, Integer> entry : entries) {
            //map里面，是id，跟数量
            Integer setmealId = entry.getKey();
            String setmealName = setmealDao.findById(setmealId).getName();

            Integer orderCount = entry.getValue();
            if (orderCount == null) {
                orderCount = 0;
            }
            BigDecimal singleCount = BigDecimal.valueOf((double) orderCount);
            double orderProportion = singleCount.divide(total, 2, BigDecimal.ROUND_HALF_UP).doubleValue();

            HotSetmealBo hotSetmealBo = HotSetmealBo.builder()
                    .setmealName(setmealName)
                    .orderCount(orderCount)
                    .orderProportion(orderProportion)
                    .build();
            hotSetmealBoList.add(hotSetmealBo);
        }

        return hotSetmealBoList;
        */
    }

    @Override
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //读取模板
        //getClass（）获取当前对象所属的Class对象
        //getClassLoader（）取得该Class对象的类装载器
        //getResourceAsStream()从classPath下查找文件，返回一个输入流
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("template/report_template.xlsx");
        //创建对象
        XSSFWorkbook excel = new XSSFWorkbook(inputStream);
        //创建sheet 表名
        XSSFSheet sheet = excel.getSheetAt(0);
        //第二行（row0是第一行）
        XSSFRow row1 = sheet.getRow(1);
        //第一格（模板里面有）
//        row1.getCell(4).setCellValue("运营数据报表");
        //第三行
        XSSFRow row2 = sheet.getRow(2);
//        row2.getCell(4).setCellValue("日期：");
        //获得当日日期
        String localDateNow = getLocalDateNow();
        row2.getCell(5).setCellValue(localDateNow);
        //第四行
        //XSSFRow row3 = sheet.getRow(3);
//        row3.getCell(3).setCellValue("会员数量统计");
        //第五行
        XSSFRow row4 = sheet.getRow(4);
        //会员数据统计
        MemberCountBo memberCountBo = getMemberCount();
        Integer todayInsertMember = memberCountBo.getTodayInsertMember();
        Integer totalInsertMember = memberCountBo.getTotalInsertMember();
//        row4.getCell(4).setCellValue("新增会员数");
        row4.getCell(5).setCellValue(todayInsertMember);
//        row4.getCell(6).setCellValue("总会员数");
        row4.getCell(7).setCellValue(totalInsertMember);
        //第六行
        XSSFRow row5 = sheet.getRow(5);
        Integer thisWeekInsertMember = memberCountBo.getThisWeekInsertMember();
        Integer thisMonthInsertMember = memberCountBo.getThisMonthInsertMember();
//        row5.getCell(4).setCellValue("本周新增会员数");
        row5.getCell(5).setCellValue(thisWeekInsertMember);
//        row5.getCell(6).setCellValue("本周新增会员数");
        row5.getCell(7).setCellValue(thisMonthInsertMember);
        //第七行
//        XSSFRow row6 = sheet.getRow(6);
//        row6.getCell(4).setCellValue("预约到诊数据统计");
        //第八行
        XSSFRow row7 = sheet.getRow(7);
        //预约到诊数据统计
        OrderCountBo orderCountBo = getOrderCount();
        Integer todayOrderCount = orderCountBo.getTodayOrderCount();
        Integer todayComeHereCount = orderCountBo.getTodayComeHereCount();
//        row7.getCell(4).setCellValue("今日预约数");
        row7.getCell(5).setCellValue(todayOrderCount);
//        row7.getCell(6).setCellValue("今日到诊数");
        row7.getCell(7).setCellValue(todayComeHereCount);
        //第九行
        XSSFRow row8 = sheet.getRow(8);
        Integer thisWeekOrderCount = orderCountBo.getThisWeekOrderCount();
        Integer thisWeekComeHereCount = orderCountBo.getThisWeekComeHereCount();
//        row8.getCell(4).setCellValue("本周预约数");
        row8.getCell(5).setCellValue(thisWeekOrderCount);
//        row8.getCell(6).setCellValue("本周到诊数");
        row8.getCell(7).setCellValue(thisWeekComeHereCount);
        //第十行
        XSSFRow row9 = sheet.getRow(9);
        Integer thisMonthOrderCount = orderCountBo.getThisMonthOrderCount();
        Integer thisMonthComeHereCount = orderCountBo.getThisMonthComeHereCount();
//        row9.getCell(4).setCellValue("本月预约数");
        row9.getCell(5).setCellValue(thisMonthOrderCount);
//        row9.getCell(6).setCellValue("本月到诊数");
        row9.getCell(7).setCellValue(thisMonthComeHereCount);
        //第十一行
//        XSSFRow row10 = sheet.getRow(10);
//        row10.getCell(4).setCellValue("热门套餐");
        //第十二行
//        XSSFRow row11 = sheet.getRow(11);
//        row11.getCell(4).setCellValue("套餐名称");
//        row11.getCell(5).setCellValue("预约数量");
//        row11.getCell(6).setCellValue("占比");
//        row11.getCell(7).setCellValue("备注");
        //热门套餐,封装
        List<HotSetmealBo> hotSetmealList = getHotSetmeal();
        //遍历
        int rowCount = 12;
        for (HotSetmealBo hotSetmealBo : hotSetmealList) {
            //第十三行//第十四行 //第十五行//第十六行
            XSSFRow row = sheet.getRow(rowCount);
            String setmealName = hotSetmealBo.getSetmealName();
            Integer orderCount = hotSetmealBo.getOrderCount();
            Double orderProportion = hotSetmealBo.getOrderProportion();
            String attention = hotSetmealBo.getAttention();
            row.getCell(4).setCellValue(setmealName);
            row.getCell(5).setCellValue(orderCount);
            row.getCell(6).setCellValue(orderProportion);
            row.getCell(7).setCellValue(attention);
            rowCount++;
        }

        //下载到客户端
        //HttpServletResponse如何根据响应文件格式设置响应头,http格式响应
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("content-Disposition", "attachment;filename=report.xlsx");
        //4、导出Excel
        ServletOutputStream out = response.getOutputStream();
        //释放资源
        excel.write(out);
        out.flush();
        out.close();
        excel.close();
    }

    /*    @Override
        public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
            //创建对象
            //InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("template/report_template.xlsx");

            //模板位置
            String filePath = "health_backend/src/main/resources/static/template/report_template.xlsx";
            //读取模板
            XSSFWorkbook excel = null;
            try {
                excel = new XSSFWorkbook(new FileInputStream(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }

            //创建sheet 表名
            XSSFSheet sheet = excel.createSheet("运营数据报表");

            //第一行
            XSSFRow row1 = sheet.getRow(1);
            //第一格
    //        row1.getCell(4).setCellValue("运营数据报表");
            //第二行
            XSSFRow row2 = sheet.getRow(2);
    //        row2.getCell(4).setCellValue("日期：");

            //获得当日日期
            String localDateNow = getLocalDateNow();
            row2.getCell(5).setCellValue(localDateNow);

            //第三行
            XSSFRow row3 = sheet.getRow(3);
    //        row3.getCell(3).setCellValue("会员数量统计");
            //第四行
            XSSFRow row4 = sheet.getRow(4);
            //会员数据统计
            MemberCountBo memberCountBo = getMemberCount();
            Integer todayInsertMember = memberCountBo.getTodayInsertMember();
            Integer totalInsertMember = memberCountBo.getTotalInsertMember();
    //        row4.getCell(4).setCellValue("新增会员数");
            row4.getCell(5).setCellValue(todayInsertMember);
    //        row4.getCell(6).setCellValue("总会员数");
            row4.getCell(7).setCellValue(totalInsertMember);
            //第五行
            XSSFRow row5 = sheet.getRow(5);
            Integer thisWeekInsertMember = memberCountBo.getThisWeekInsertMember();
            Integer thisMonthInsertMember = memberCountBo.getThisMonthInsertMember();
    //        row5.getCell(4).setCellValue("本周新增会员数");
            row5.getCell(5).setCellValue(thisWeekInsertMember);
    //        row5.getCell(6).setCellValue("本周新增会员数");
            row5.getCell(7).setCellValue(thisMonthInsertMember);

            //第六行
    //        XSSFRow row6 = sheet.getRow(6);
    //        row6.getCell(4).setCellValue("预约到诊数据统计");
            //第七行
            XSSFRow row7 = sheet.getRow(7);
            //预约到诊数据统计
            OrderCountBo orderCountBo = getOrderCount();
            Integer todayOrderCount = orderCountBo.getTodayOrderCount();
            Integer todayComeHereCount = orderCountBo.getTodayComeHereCount();
    //        row7.getCell(4).setCellValue("今日预约数");
            row7.getCell(5).setCellValue(todayOrderCount);
    //        row7.getCell(6).setCellValue("今日到诊数");
            row7.getCell(7).setCellValue(todayComeHereCount);
            //第八行
            XSSFRow row8 = sheet.getRow(8);
            Integer thisWeekOrderCount = orderCountBo.getThisWeekOrderCount();
            Integer thisWeekComeHereCount = orderCountBo.getThisWeekComeHereCount();
    //        row8.getCell(4).setCellValue("本周预约数");
            row8.getCell(5).setCellValue(thisWeekOrderCount);
    //        row8.getCell(6).setCellValue("本周到诊数");
            row8.getCell(7).setCellValue(thisWeekComeHereCount);

            //第九行
            XSSFRow row9 = sheet.getRow(9);
            Integer thisMonthOrderCount = orderCountBo.getThisMonthOrderCount();
            Integer thisMonthComeHereCount = orderCountBo.getThisMonthComeHereCount();
    //        row9.getCell(4).setCellValue("本月预约数");
            row9.getCell(5).setCellValue(thisMonthOrderCount);
    //        row9.getCell(6).setCellValue("本月到诊数");
            row9.getCell(7).setCellValue(thisMonthComeHereCount);

            //第十行
    //        XSSFRow row10 = sheet.getRow(10);
    //        row10.getCell(4).setCellValue("热门套餐");

            //第十一行
    //        XSSFRow row11 = sheet.getRow(11);
    //        row11.getCell(4).setCellValue("套餐名称");
    //        row11.getCell(5).setCellValue("预约数量");
    //        row11.getCell(6).setCellValue("占比");
    //        row11.getCell(7).setCellValue("备注");

            //热门套餐,封装
            List<HotSetmealBo> hotSetmealList = getHotSetmeal();
            //遍历
            int rowCount = 12;
            for (HotSetmealBo hotSetmealBo : hotSetmealList) {
                //第十二行//第十三行//第十四行 //第十五行
                XSSFRow row = sheet.getRow(rowCount);
                String setmealName = hotSetmealBo.getSetmealName();
                Integer orderCount = hotSetmealBo.getOrderCount();
                Double orderProportion = hotSetmealBo.getOrderProportion();
                String attention = hotSetmealBo.getAttention();
                row.getCell(4).setCellValue(setmealName);
                row.getCell(5).setCellValue(orderCount);
                row.getCell(6).setCellValue(orderProportion);
                row.getCell(7).setCellValue(attention);
                rowCount++;
            }

            //下载到客户端
            ServletOutputStream out = response.getOutputStream();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("content-Disposition", "attachment;filename=report.xlsx");

            excel.write(out);
            out.flush();
            out.close();
            excel.close();

        }*/
/*    @Override
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //创建对象
        XSSFWorkbook excel = new XSSFWorkbook();
        //创建sheet 表名
        XSSFSheet sheet = excel.createSheet("运营数据报表");
        //第一行
        XSSFRow row0 = sheet.getRow(0);
        //第一格
        row0.getCell(0).setCellValue("运营数据报表");
        //第二行
        XSSFRow row1 = sheet.getRow(1);
        row1.getCell(0).setCellValue("日期：");
        //获得当日日期
        String localDateNow = getLocalDateNow();
        row1.getCell(1).setCellValue(localDateNow);
        //第三行
        XSSFRow row2 = sheet.getRow(2);
        row2.getCell(0).setCellValue("会员数量统计");
        //第四行
        XSSFRow row3 = sheet.getRow(3);
        //会员数据统计
        MemberCountBo memberCountBo = getMemberCount();
        Integer todayInsertMember = memberCountBo.getTodayInsertMember();
        Integer totalInsertMember = memberCountBo.getTotalInsertMember();
        row3.getCell(0).setCellValue("新增会员数");
        row3.getCell(1).setCellValue(todayInsertMember);
        row3.getCell(2).setCellValue("总会员数");
        row3.getCell(3).setCellValue(totalInsertMember);
        //第五行
        XSSFRow row4 = sheet.getRow(4);
        Integer thisWeekInsertMember = memberCountBo.getThisWeekInsertMember();
        Integer thisMonthInsertMember = memberCountBo.getThisMonthInsertMember();
        row4.getCell(0).setCellValue("本周新增会员数");
        row4.getCell(1).setCellValue(thisWeekInsertMember);
        row4.getCell(2).setCellValue("本周新增会员数");
        row4.getCell(3).setCellValue(thisMonthInsertMember);

        //第六行
        XSSFRow row5 = sheet.getRow(5);
        row5.getCell(0).setCellValue("预约到诊数据统计");
        //第七行
        XSSFRow row6 = sheet.getRow(6);
        //预约到诊数据统计
        OrderCountBo orderCountBo = getOrderCount();
        Integer todayOrderCount = orderCountBo.getTodayOrderCount();
        Integer todayComeHereCount = orderCountBo.getTodayComeHereCount();
        row6.getCell(0).setCellValue("今日预约数");
        row6.getCell(1).setCellValue(todayOrderCount);
        row6.getCell(2).setCellValue("今日到诊数");
        row6.getCell(3).setCellValue(todayComeHereCount);
        //第八行
        XSSFRow row7 = sheet.getRow(7);
        Integer thisWeekOrderCount = orderCountBo.getThisWeekOrderCount();
        Integer thisWeekComeHereCount = orderCountBo.getThisWeekComeHereCount();
        row7.getCell(0).setCellValue("本周预约数");
        row7.getCell(1).setCellValue(thisWeekOrderCount);
        row7.getCell(2).setCellValue("本周到诊数");
        row7.getCell(3).setCellValue(thisWeekComeHereCount);

        //第九行
        XSSFRow row8 = sheet.getRow(8);
        Integer thisMonthOrderCount = orderCountBo.getThisMonthOrderCount();
        Integer thisMonthComeHereCount = orderCountBo.getThisMonthComeHereCount();
        row8.getCell(0).setCellValue("本月预约数");
        row8.getCell(1).setCellValue(thisMonthOrderCount);
        row8.getCell(2).setCellValue("本月到诊数");
        row8.getCell(3).setCellValue(thisMonthComeHereCount);

        //第十行
        XSSFRow row9 = sheet.getRow(9);
        row9.getCell(0).setCellValue("热门套餐");

        //第十一行
        XSSFRow row10 = sheet.getRow(10);
        row10.getCell(0).setCellValue("套餐名称");
        row10.getCell(1).setCellValue("预约数量");
        row10.getCell(2).setCellValue("占比");
        row10.getCell(3).setCellValue("备注");

        //热门套餐,封装
        List<HotSetmealBo> hotSetmealList = getHotSetmeal();
        //遍历
        int rowCount = 11;
        for (HotSetmealBo hotSetmealBo : hotSetmealList) {
            //第十二行//第十三行//第十四行 //第十五行
            XSSFRow row = sheet.getRow(rowCount);
            String setmealName = hotSetmealBo.getSetmealName();
            Integer orderCount = hotSetmealBo.getOrderCount();
            Double orderProportion = hotSetmealBo.getOrderProportion();
            String attention = hotSetmealBo.getAttention();
            row.getCell(0).setCellValue(setmealName);
            row.getCell(1).setCellValue(orderCount);
            row.getCell(2).setCellValue(orderProportion);
            row.getCell(3).setCellValue(attention);
            rowCount++;
        }

        //导出
        FileOutputStream fos = new FileOutputStream(new File("D:/AAA报表.xlsx"));
        excel.write(fos);
        fos.flush();
        fos.close();
        excel.close();

    }*/
    //下载为PDF
    @Override
    public void exportPDF(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //resources里面fontResource放的是指定的字体（中文）
        //这个是测试模板
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jasper/Blank_A4.jasper");
        //这个map是要装的参数（获取数据源）(都装到这个map里面)
        Map params = new HashMap<>();

        Map<String, Object> map = getBusinessReport();
        String reportDate = (String) map.get("reportDate");

        params.put("reportDate", reportDate);

        List<HotSetmealBo> hotSetmeal = (List<HotSetmealBo>) map.get("hotSetmeal");
        HotSetmealBo hotSetmealBo = hotSetmeal.get(0);
        String setmealName1 = hotSetmealBo.getSetmealName();
        Integer orderCount1 = hotSetmealBo.getOrderCount();
        Double orderProportion1 = hotSetmealBo.getOrderProportion();
        String attention1 = hotSetmealBo.getAttention();

        HotSetmealBo hotSetmealBo2 = hotSetmeal.get(1);
        String setmealName2 = hotSetmealBo2.getSetmealName();
        Integer orderCount2 = hotSetmealBo2.getOrderCount();
        Double orderProportion2 = hotSetmealBo2.getOrderProportion();
        String attention2 = hotSetmealBo2.getAttention();

        params.put("setmealName1", setmealName1);
        params.put("orderCount1", orderCount1);
        params.put("orderProportion1", orderProportion1);
        params.put("attention1", attention1);

        params.put("setmealName2", setmealName2);
        params.put("orderCount2", orderCount2);
        params.put("orderProportion2", orderProportion2);
        params.put("attention2", attention2);

        //bug定位
        JasperPrint jasperPrint = null;
        try {
            jasperPrint = JasperFillManager.fillReport(inputStream, params, new JREmptyDataSource());
        } catch (JRException e) {
            e.printStackTrace();
        }
        //指定下载的位置
        ServletOutputStream out = response.getOutputStream();
        response.setContentType("application/vnd.ms-pdf");
        response.setHeader("content-Disposition", "attachment;filename=report.pdf");
        try {
            //JRException: java.io.IOException: The byte array is not a recognized imageformat.
            JasperExportManager.exportReportToPdfStream(jasperPrint, out);
        } catch (JRException e) {
            e.printStackTrace();
            throw e;
        }
        out.flush();
        out.close();
    }
/*    //下载为PDF
    @Override
    public void exportPDF(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //模板文件位置
        //resources里面fontResource放的是指定的字体（中文）
        //String filePath = "health_backend/src/main/resources/jasper/数据报表.jasper";
        String filePath = "health_backend/src/main/resources/jasper/Blank_A4.jasper";
        FileInputStream fis = new FileInputStream(filePath);
        //这个map是要装的参数
        Map params = new HashMap<>();
        Map<String, Object> map = getBusinessReport();
        String reportDate = (String) map.get("reportDate");
        params.put("reportDate", reportDate);

        List<HotSetmealBo> hotSetmeal = (List<HotSetmealBo>) map.get("hotSetmeal");
        HotSetmealBo hotSetmealBo = hotSetmeal.get(0);
        String setmealName1 = hotSetmealBo.getSetmealName();
        Integer orderCount1 = hotSetmealBo.getOrderCount();
        Double orderProportion1 = hotSetmealBo.getOrderProportion();
        String attention1 = hotSetmealBo.getAttention();

        HotSetmealBo hotSetmealBo2 = hotSetmeal.get(1);
        String setmealName2 = hotSetmealBo2.getSetmealName();
        Integer orderCount2 = hotSetmealBo2.getOrderCount();
        Double orderProportion2 = hotSetmealBo2.getOrderProportion();
        String attention2 = hotSetmealBo2.getAttention();

        params.put("setmealName1", setmealName1);
        params.put("orderCount1", orderCount1);
        params.put("orderProportion1", orderProportion1);
        params.put("attention1", attention1);

        params.put("setmealName2", setmealName2);
        params.put("orderCount2", orderCount2);
        params.put("orderProportion2", orderProportion2);
        params.put("attention2", attention2);

//      result.put("reportDate", today);
//        result.put("todayNewMember", todayNewMember);
//        result.put("totalMember", totalMember);
//        result.put("thisWeekNewMember", thisWeekNewMember);
//        result.put("thisMonthNewMember", thisMonthNewMember);
//        result.put("todayOrderNumber", todayOrderNumber);
//        result.put("thisWeekOrderNumber", thisWeekOrderNumber);
//        result.put("thisMonthOrderNumber", thisMonthOrderNumber);
//        result.put("todayVisitsNumber", todayVisitsNumber);
//        result.put("thisWeekVisitsNumber", thisWeekVisitsNumber);
//        result.put("thisMonthVisitsNumber", thisMonthVisitsNumber);
//        result.put("hotSetmeal", hotSetmeal);

        //bug定位
        JasperPrint jasperPrint = null;
        try {
            jasperPrint = JasperFillManager.fillReport(fis, params, new JREmptyDataSource());
        } catch (JRException e) {
            e.printStackTrace();
        }
        //指定下载的位置
        FileOutputStream fos = new FileOutputStream("D:/AAA报表数据.pdf");
        try {
            //JRException: java.io.IOException: The byte array is not a recognized imageformat.
            JasperExportManager.exportReportToPdfStream(jasperPrint, fos);
        } catch (JRException e) {
            e.printStackTrace();
            throw e;
        }
    }*/

    //封装为map集合<String，List>
    @Override
    public Map setmealReportTransformToMap(Map<String, Double> setmealNamesAndSetmealCount) {
        //map.put("setmealNames",setmealNames);
        // 返回结果:setmealCount  setmealCount
        Map map = new HashMap<>();
        List<String> setmealNames = new ArrayList();
        List<Double> setmealCount = new ArrayList();
        Set<Map.Entry<String, Double>> entries = setmealNamesAndSetmealCount.entrySet();
        for (Map.Entry<String, Double> entry : entries) {
            String key = entry.getKey();
            Double value = entry.getValue();
            setmealNames.add(key);
            setmealCount.add(value);
        }
        map.put("setmealNames", setmealNames);
        map.put("setmealCount", setmealCount);
        return map;
    }

    //获取运营数据统计，封装为map
    @Override
    public Map businessReportDataTransformToMap(Map<String, Object> businessReport) {
        Map map = new HashMap();

        Object date = businessReport.get("reportDate");
        Object todayNewMember = businessReport.get("todayNewMember");
        Object totalMember = businessReport.get("totalMember");
        Object thisWeekNewMember = businessReport.get("thisWeekNewMember");
        Object thisMonthNewMember = businessReport.get("thisMonthNewMember");
        Object todayOrderNumber = businessReport.get("todayOrderNumber");
        Object thisWeekOrderNumber = businessReport.get("thisWeekOrderNumber");
        Object thisMonthOrderNumber = businessReport.get("thisMonthOrderNumber");
        Object todayVisitsNumber = businessReport.get("todayVisitsNumber");
        Object thisWeekVisitsNumber = businessReport.get("thisWeekVisitsNumber");
        Object thisMonthVisitsNumber = businessReport.get("thisMonthVisitsNumber");
        //不一致
        List<HotSetmealBo> hotSetmeal = (List<HotSetmealBo>) businessReport.get("hotSetmeal");
        List hotSetmealList = new ArrayList();
        for (HotSetmealBo hotSetmealBo : hotSetmeal) {
            Map map1 = new HashMap();
            String setmealName = hotSetmealBo.getSetmealName();
            Integer orderCount = hotSetmealBo.getOrderCount();
            Double orderProportion = hotSetmealBo.getOrderProportion();
            String attention = hotSetmealBo.getAttention();
            map1.put("name", setmealName);
            map1.put("setmeal_count", orderCount);
            map1.put("proportion", orderProportion);
            map1.put("attention", attention);
            hotSetmealList.add(map1);
        }

        map.put("reportDate", date);
        map.put("todayNewMember", todayNewMember);
        map.put("totalMember", totalMember);
        map.put("thisWeekNewMember", thisWeekNewMember);

        map.put("thisMonthNewMember", thisMonthNewMember);
        map.put("todayOrderNumber", todayOrderNumber);

        map.put("thisWeekOrderNumber", thisWeekOrderNumber);
        map.put("thisMonthOrderNumber", thisMonthOrderNumber);
        map.put("todayVisitsNumber", todayVisitsNumber);
        map.put("thisWeekVisitsNumber", thisWeekVisitsNumber);
        map.put("thisMonthVisitsNumber", thisMonthVisitsNumber);
        map.put("hotSetmeal", hotSetmealList);
        return map;
    }
}
