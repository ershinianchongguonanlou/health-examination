package com.itheima.service.impl;

import com.itheima.dao.MemberDao;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import com.itheima.utils.MD5Utils;
import com.itheima.utils.MonthUtils;
import com.itheima.Vo.MemberRegVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员服务
 */
@Service
@Transactional
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;

    //根据手机号查询会员
    public Member findByTelephone(String telephone) {
        return memberDao.findByTelephone(telephone);
    }

    //新增会员
    public void add(Member member) {
        if (member.getPassword() != null) {
            member.setPassword(MD5Utils.md5(member.getPassword()));
        }
        memberDao.add(member);
    }

    //会员统计--获取日期
    @Override
    public List<LocalDate> getDateTable() {
        //先格式化
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM");
        //获取当前年月
        LocalDate now = LocalDate.now();
//        String end = now.format(formatter);
        //获取11月前年月 -11
        LocalDate localDate = now.minusMonths(11);
//        String start = localDate.format(formatter);
//        //死循环方式向里面放数据
//        String[] dateTable = new String[12];
//        dateTable[0] = start;
//        for (int i = 1; i < dateTable.length; i++) {
//            localDate = localDate.plusMonths(1);
//            dateTable[i] = localDate.format(formatter);
//        }
        ArrayList<LocalDate> dateTable = new ArrayList<>();
        dateTable.add(localDate);
        while (!localDate.equals(now)) {
            localDate = localDate.plusMonths(1);
            dateTable.add(localDate);
        }
        //System.out.println("dateTable = " + dateTable);
        //[2022.09, 2022.10, 2022.11, 2022.12, 2023.01, 2023.02,
        // 2023.03, 2023.04, 2023.05, 2023.06, 2023.07, 2023.08]
        //System.out.println("Arrays.toString(dateTable) = " + Arrays.toString(dateTable));
        return dateTable;
    }

    //根据月份查询注册会员总数量
    @Override
    public List<MemberRegVo> getRegTime(List<LocalDate> dateTable) throws Exception {

        List<Integer> countMember = new ArrayList<>();
        List<String> monthList = new ArrayList<>();
        for (LocalDate localDate : dateTable) {
            //获取一个月第一天
            LocalDate start = MonthUtils.getFirstDayOfMonth(localDate);
            //获取一个月最后一天
            LocalDate end = MonthUtils.getLastDayOfMonth(localDate);
            // memberCount
            Integer count = memberDao.findCountByRegTime(start, end);
            countMember.add(count);
            // 格式化
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM");
            // months
            String month = localDate.format(formatter);
            monthList.add(month);
        }
        if (monthList.size() != countMember.size()) {
            throw new Exception("会员统计查询异常");
        }
        //封装
        List<MemberRegVo> memberRegVoList = new ArrayList<>();
        for (int i = 0; i < monthList.size(); i++) {
            String months = monthList.get(i);
            Integer memberCount = countMember.get(i);
            memberRegVoList.add(new MemberRegVo(months, memberCount));
        }

        return memberRegVoList;
    }

    //会员统计-- //封装为map集合<List，List>
    @Override
    public Map transformToMap(List<MemberRegVo> memberRegVoList) {
        Map map = new HashMap<>();
        List<String> months = new ArrayList<>();
        List<Integer> memberCount = new ArrayList<>();
        for (MemberRegVo memberRegVo : memberRegVoList) {
            String memberRegVoMonth = memberRegVo.getMonths();
            Integer memberRegVoMemberCount = memberRegVo.getMemberCount();
            months.add(memberRegVoMonth);
            memberCount.add(memberRegVoMemberCount);
        }
        //装入map
        map.put("months", months);
        map.put("memberCount", memberCount);
        return map;
    }

}
