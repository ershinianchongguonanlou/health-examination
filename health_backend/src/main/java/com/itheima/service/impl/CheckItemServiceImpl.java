package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.dao.SetmealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckGroupService;
import com.itheima.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckItemServiceImpl
 * @Description: 检查项管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:07
 */
@Service
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;

    @Autowired
    private CheckGroupDao checkGroupDao;

    @Autowired
    private SetmealDao setmealDao;

    //查询所有
    @Override
    public List<CheckItem> findAll() {
        List<CheckItem> checkItemList = checkItemDao.findAll();
        return checkItemList;
    }

    //根据id查询
    @Override
    public CheckItem findById(Integer id) {
        CheckItem checkItem = checkItemDao.findById(id);
        return checkItem;
    }

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        if (currentPage == null) {
            currentPage = 1;
        }
        Integer pageSize = queryPageBean.getPageSize();
        if (pageSize == null) {
            pageSize = 10;
        }
        //条件查询，只有一条数据，分页limit 0，X （起始索引从零开始）
        String queryString = queryPageBean.getQueryString();
//        if (queryString != null) {
//            currentPage = 0;
//        }
        PageHelper.startPage(currentPage, pageSize);
        List<CheckItem> checkItemList = checkItemDao.findByCondition(queryString);
        Page<CheckItem> checkItemPage = (Page<CheckItem>) checkItemList;
        long total = checkItemPage.getTotal();
        List<CheckItem> rows = checkItemPage.getResult();
        return new PageResult(total, rows);
    }

    //修改
    @Override
    public void edit(CheckItem checkItem) {
        checkItemDao.edit(checkItem);
    }

    //根据ID删除
    @Override
    public void deleteById(Integer id) {
        //根据项id查询管理的组
        List<Integer> CheckGroupIdList = checkItemDao.findThirdByCheckItemId(id);
        //有关联，删除组
        if (CheckGroupIdList != null) {
            if (CheckGroupIdList.size() > 0) {
                //删除第三方表
                checkItemDao.deleteThreadByCiId(id);
                for (Integer checkGroupId : CheckGroupIdList) {
                    //第三方表，组清除与其它项
                    checkGroupDao.deleteThirdTableByCgId(checkGroupId);
                    //根据组ID删除组
                    deleteByCheckGroupId(checkGroupId);
                }
            }
        }
        //无关联，删除项表
        checkItemDao.deleteById(id);
    }

    //根据组ID删除组
    private void deleteByCheckGroupId(Integer checkGroupId) {
        //先找关联套餐,根据组id，查询关联套餐id
        List<Integer> setmaelIdList = setmealDao.findThirdByCheckGroupId(checkGroupId);
        //有则删除
        if (setmaelIdList != null) {
            if (setmaelIdList.size() > 0) {
                //删除第三方表,根据组id，删除第三方表（组_套餐）
                checkGroupDao.deleteOtherThirdTableByCgId(checkGroupId);
                //套餐解绑，与其它组关联
                setmealDao.deleteOtherThirdTableByIds(setmaelIdList);
                //删除套餐，批量删除
                setmealDao.deleteByIds(setmaelIdList);
            }
        }
        //无有则直接删除
        checkGroupDao.deleteById(checkGroupId);
    }

    //新增
    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.insert(checkItem);
    }
}
