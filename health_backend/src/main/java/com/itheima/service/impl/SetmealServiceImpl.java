package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.dao.SetmealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: SetmealServiceImpl
 * @Description: 套餐管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:12
 */
@Service
@Transactional
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealDao setmealDao;

    @Autowired
    private CheckGroupDao checkGroupDao;

    @Autowired
    private CheckItemDao checkItemDao;

    //查询所有
    @Override
    public List<Setmeal> findAll() {
        List<Setmeal> setmealList = setmealDao.findAll();
        //拿组
        for (Setmeal setmeal : setmealList) {
            Integer id = setmeal.getId();
            List<CheckGroup> checkGroupList = getCheckGroupsBySetmealId(id);
            //封装回去
            setmeal.setCheckGroups(checkGroupList);
        }
        return setmealList;
    }

    //根据id查询
    @Override
    public Setmeal findById(Integer id) {
        Setmeal setmeal = setmealDao.findById(id);
        //检查组拿出来
        List<CheckGroup> checkGroupList = getCheckGroupsBySetmealId(id);
        setmeal.setCheckGroups(checkGroupList);
        return setmeal;
    }

    /**
     * 根据套餐id，拿到下面的检查组
     *
     * @param id 套餐id
     * @return 检查组集合
     */
    private List<CheckGroup> getCheckGroupsBySetmealId(Integer id) {
        //第三方表，根据套餐id，拿组id
        List<Integer> checkGroupIds = setmealDao.findThirdBysId(id);
        //根据组ID拿组,放到集合中
        List<CheckGroup> checkGroupList = new ArrayList<>();
        for (Integer checkGroupId : checkGroupIds) {
            CheckGroup checkGroup = checkGroupDao.findById(checkGroupId);
            //解决"checkItems": null
            List<Integer> checkItemIds = checkItemDao.findThirdByCheckGroupId(checkGroupId);
            List<CheckItem> checkItemList = new ArrayList<>();
            for (Integer checkItemId : checkItemIds) {
                CheckItem checkItem = checkItemDao.findById(checkItemId);
                checkItemList.add(checkItem);
            }
            checkGroup.setCheckItems(checkItemList);
            checkGroupList.add(checkGroup);
        }
        return checkGroupList;
    }

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        if (currentPage == null) {
            currentPage = 1;
        }
        Integer pageSize = queryPageBean.getPageSize();
        if (pageSize == null) {
            pageSize = 2;
        }
        PageHelper.startPage(currentPage, pageSize);
        String queryString = queryPageBean.getQueryString();
        List<Setmeal> setmealList = setmealDao.findByCondition(queryString);
        //我们发现 "checkGroups": null
        for (Setmeal setmeal : setmealList) {
            Integer id = setmeal.getId();
            List<CheckGroup> checkGroupList = getCheckGroupsBySetmealId(id);
            setmeal.setCheckGroups(checkGroupList);
        }
        Page<Setmeal> setmealPage = (Page<Setmeal>) setmealList;
        long total = setmealPage.getTotal();
        List<Setmeal> rows = setmealPage.getResult();
        return new PageResult(total, rows);
    }

    //新增
    @Override
    public void add(Setmeal setmeal) {
        //新增套餐
        setmealDao.insert(setmeal);
        //主键回显
        Integer sId = setmeal.getId();
        List<CheckGroup> checkGroups = setmeal.getCheckGroups();
        for (CheckGroup checkGroup : checkGroups) {
            Integer cgId = checkGroup.getId();
            //第三方表
            setmealDao.insertThird(sId, cgId);
        }
    }

    //批量，根据ID删除
    @Override
    public void deleteById(Integer id) {
        //删掉第三方关联表，根据套餐id、解绑
        List<Integer> setmaelIdList = new ArrayList<>();
        setmaelIdList.add(id);
        setmealDao.deleteOtherThirdTableByIds(setmaelIdList);
        //删掉套餐表数据
        setmealDao.deleteByIds(setmaelIdList);
    }

}
