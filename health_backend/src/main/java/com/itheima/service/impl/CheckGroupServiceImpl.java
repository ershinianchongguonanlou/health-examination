package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.dao.SetmealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckGroupServiceImpl
 * @Description:
 * @Author: HeKuo
 * @Date: 2023/8/1017:09
 */
@Service
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;

    @Autowired
    private CheckItemDao checkItemDao;

    @Autowired
    private SetmealDao setmealDao;

    //查询所有
    @Override
    public List<CheckGroup> findAll() {
        //查询组(集合)
        List<CheckGroup> checkGroupList = checkGroupDao.findAll();
        //遍历集合
        for (CheckGroup checkGroup : checkGroupList) {
            Integer id = checkGroup.getId();
            //根据组ID，查询关联的项，（抽取成方法）
            List<CheckItem> checkItems = getCheckItemsByCheckGroupId(id);
            checkGroup.setCheckItems(checkItems);
        }
        return checkGroupList;
    }

    //根据id查询
    @Override
    public CheckGroup findById(Integer id) {
        CheckGroup checkGroup = checkGroupDao.findById(id);
        //我们发现 "checkItems": null
        //查询第三方表
        List<CheckItem> checkItemList = getCheckItemsByCheckGroupId(id);
        checkGroup.setCheckItems(checkItemList);
        return checkGroup;
    }

    /**
     * 根据检查组ID，拿到下面的检查项IDs
     *
     * @param id 检查组ID
     * @return 检查项集合
     */
    private List<CheckItem> getCheckItemsByCheckGroupId(Integer id) {
        List<Integer> ciIds = checkGroupDao.findThirdBycgId(id);
        List<CheckItem> checkItemList = new ArrayList<>();
        for (Integer ciId : ciIds) {
            CheckItem checkItem = checkItemDao.findById(ciId);
            checkItemList.add(checkItem);
        }
        return checkItemList;
    }

    //根据组id查询对应的项
    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
        List<CheckItem> checkItemList = checkGroupDao.findCheckItemIdsByCheckGroupId(id);
        //遍历返回，ID（后期修改，修改源，可能其它引用错误）
        List<Integer> checkItemIdList = new ArrayList<>();
        for (CheckItem checkItem : checkItemList) {
            Integer checkItemId = checkItem.getId();
            checkItemIdList.add(checkItemId);
        }
        return checkItemIdList;
    }

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        if (currentPage == null) {
            currentPage = 1;
        }
        Integer pageSize = queryPageBean.getPageSize();
        if (pageSize == null) {
            pageSize = 5;
        }
        PageHelper.startPage(currentPage, pageSize);
        String queryString = queryPageBean.getQueryString();

        List<CheckGroup> checkGroupList = checkGroupDao.findByCondition(queryString);
        //解决 "checkItems": null问题
        for (CheckGroup checkGroup : checkGroupList) {
            Integer id = checkGroup.getId();
            List<CheckItem> checkItemList = getCheckItemsByCheckGroupId(id);
            checkGroup.setCheckItems(checkItemList);
        }

        Page<CheckGroup> checkGroupPage = (Page<CheckGroup>) checkGroupList;
        long total = checkGroupPage.getTotal();
        List<CheckGroup> rows = checkGroupPage.getResult();
        return new PageResult(total, rows);
    }

    //修改
    @Override
    public void edit(CheckGroup checkGroup, List<Integer> checkitemIds) {
        //修改CG组表
        checkGroupDao.edit(checkGroup);
        //修改第三方表
        //先删掉第三方表
        Integer cgId = checkGroup.getId();
        checkGroupDao.deleteThirdTableByCgId(cgId);
        //在新增
        for (Integer ciId : checkitemIds) {
            checkGroupDao.insertThirdTable(cgId, ciId);
        }
    }

    //新增
    @Override
    public void add(CheckGroup checkGroup, List<Integer> checkitemIds) {
        //新增CG组表
        checkGroupDao.insert(checkGroup);
        //checkItems没传进来
        //主键回显
        Integer cgId = checkGroup.getId();
        //新增第三方表
        for (Integer ciId : checkitemIds) {
            checkGroupDao.insertThirdTable(cgId, ciId);
        }
    }

    //根据ID删除
    @Override
    public void deleteById(Integer id) {
        //第三方解绑（组_项）
        checkGroupDao.deleteThirdTableByCgId(id);
        //查询关联套餐id
        List<Integer> selmealIdList = setmealDao.findThirdByCheckGroupId(id);
        if (selmealIdList != null) {
            if (selmealIdList.size() > 0) {
                //有则删除，套餐解绑，删除组_项第三方表(组_套餐)
                checkGroupDao.deleteThirdTableByCgId(id);
                setmealDao.deleteOtherThirdTableByIds(selmealIdList);
                //删除套餐
                setmealDao.deleteByIds(selmealIdList);
            }
        }
        //无则，删除组
        checkGroupDao.deleteById(id);
    }
}

