package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckItemService
 * @Description: 检查项管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:06
 */
public interface CheckItemService {
    //查询所有
    List<CheckItem> findAll();

    ////根据id查询
    CheckItem findById(Integer id);

    //分页查询
    PageResult findPage(QueryPageBean queryPageBean);

    //修改
    void edit(CheckItem checkItem);

    //根据ID删除
    void deleteById(Integer id);

    //新增
    void add(CheckItem checkItem);
}
