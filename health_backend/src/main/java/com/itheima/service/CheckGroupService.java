package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckGroupService
 * @Description: TODO
 * @Author: HeKuo
 * @Date: 2023/8/1017:09
 */
public interface CheckGroupService {

    //查询所有
    List<CheckGroup> findAll();

    //根据id查询
    CheckGroup findById(Integer id);

    //根据组id查询对应的项
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    //分页查询
    PageResult findPage(QueryPageBean queryPageBean);

    //修改
    void edit(CheckGroup checkGroup, List<Integer> checkitemIds);

    //新增
    void add(CheckGroup checkGroup, List<Integer> checkitemIds);

    //根据ID删除
    void deleteById(Integer id);
}
