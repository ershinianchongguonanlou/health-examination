package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: SetmealService
 * @Description: 套餐管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:12
 */
public interface SetmealService {

    //查询所有
    List<Setmeal> findAll();

    //根据id查询
    Setmeal findById(Integer id);

    //分页查询
    PageResult findPage(QueryPageBean queryPageBean);

    //新增
    void add(Setmeal setmeal);

    //根据ID删除
    void deleteById(Integer id);
}
