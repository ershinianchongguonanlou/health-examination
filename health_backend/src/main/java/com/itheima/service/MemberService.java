package com.itheima.service;

import com.itheima.pojo.Member;
import com.itheima.Vo.MemberRegVo;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface MemberService {
    public Member findByTelephone(String telephone);

    public void add(Member member);

    //会员统计--获取日期
    List<LocalDate> getDateTable();

    //会员统计
    List<MemberRegVo> getRegTime(List<LocalDate> dateTable) throws Exception;

    //会员统计-- //封装为map集合<List，List>
    Map transformToMap(List<MemberRegVo> memberRegVoList);

    //String[] getDateTable();

}
