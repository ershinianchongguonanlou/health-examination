//package com.itheima.security;
//
//
//import com.itheima.pojo.Permission;
//import com.itheima.pojo.Role;
//import com.itheima.pojo.User;
//import com.itheima.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//@Component//("abc")
//public class SpringSecurityUserService implements UserDetailsService{
//    @Autowired //注意：此处要通过dubbo远程调用用户服务
//    private UserService userService;
//    //根据页面输入的用户名查询数据库中的用户信息
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        if(StringUtils.isEmpty(username)){
//            return null;
//        }
//        User user = userService.findByUsername(username);
//        if(user == null){
//            return null;
//        }
//
//        List<GrantedAuthority> list = new ArrayList<>();
//
//        //为用户动态授权
//        Set<Role> roles = user.getRoles();
//        if(roles != null && roles.size() > 0){
//            for (Role role : roles) {
//                //为当前用户授予角色
//                list.add(new SimpleGrantedAuthority(role.getKeyword()));
//                Set<Permission> permissions = role.getPermissions();
//                if(permissions != null && permissions.size() > 0){
//                    for (Permission permission : permissions) {
//                        //为当前用户授予权限
//                        list.add(new SimpleGrantedAuthority(permission.getKeyword()));
//                    }
//                }
//            }
//        }
//        return new org.springframework.security.core.userdetails.User(username,user.getPassword(),list);
//    }
//}
