package com.itheima.dao;

import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckGroupDao
 * @Description: TODO
 * @Author: HeKuo
 * @Date: 2023/8/1017:10
 */
@Mapper
public interface CheckGroupDao {
    //查询所有
    @Select("select * from t_checkgroup")
    List<CheckGroup> findAll();

    //根据id查询
    @Select("select * from t_checkgroup where id=#{id}")
    CheckGroup findById(Integer id);

    //根据组id查询对应的项
    @Select("select * from t_checkitem where id in (select tcc.checkitem_id from t_checkgroup tcg join t_checkgroup_checkitem tcc on tcg.id = tcc.checkgroup_id where tcg.id=#{id} )")
    List<CheckItem> findCheckItemIdsByCheckGroupId(Integer id);

    //根据条件查询
    List<CheckGroup> findByCondition(@Param("queryString") String queryString);

    //修改
    void edit(CheckGroup checkGroup);

    //新增
    void insert(CheckGroup checkGroup);

    //新增第三分表
    void insertThirdTable(@Param("cgId") Integer cgId, @Param("ciId") Integer ciId);

    //先删掉第三方表
    @Delete("delete from t_checkgroup_checkitem  where t_checkgroup_checkitem.checkgroup_id=#{cgId}")
    void deleteThirdTableByCgId(@Param("cgId") Integer cgId);

    //根据组ID，拿到项的ids
    @Select("select checkitem_id from t_checkgroup_checkitem where checkgroup_id=#{id}")
    List<Integer> findThirdBycgId(Integer id);

    //根据组ID，删除组数据
    @Delete("delete from t_checkgroup where id=#{id}")
    void deleteById(Integer id);

    //删除组_套餐第三方表
    @Delete("delete from t_setmeal_checkgroup where checkgroup_id=#{id}")
    void deleteOtherThirdTableByCgId(@Param("id") Integer id);

    //根据组id，查关联的套餐ID
    @Select("select * from t_setmeal_checkgroup where checkgroup_id=#{id}")
    List<Integer> findOtherThirdTableByCgId(Integer id);
}
