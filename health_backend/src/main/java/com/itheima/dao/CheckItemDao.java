package com.itheima.dao;

import com.itheima.pojo.CheckItem;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckItemDao
 * @Description: 检查项管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:07
 */
@Mapper
public interface CheckItemDao {

    //查询所有
    @Select("select * from t_checkitem")
    List<CheckItem> findAll();

    //根据id查询
    @Select("select * from t_checkitem where id=#{id}")
    CheckItem findById(@Param("id") Integer id);

    //分页查询
    List<CheckItem> findByCondition(@Param("queryString") String queryString);

    //修改
    void edit(CheckItem checkItem);

    //根据ID删除
    @Delete("delete from t_checkitem where id=#{id}")
    void deleteById(@Param("id") Integer id);

    //新增
    void insert(CheckItem checkItem);

    //根据项id，删除第三方表
    @Delete("delete from t_checkgroup_checkitem where checkitem_id=#{ciId};")
    void deleteThreadByCiId(@Param("ciId") Integer ciId);

    //根据组id，拿到项ids
    @Select("select checkitem_id from t_checkgroup_checkitem where checkgroup_id=#{checkGroupId}")
    List<Integer> findThirdByCheckGroupId(Integer checkGroupId);

    //根据项id查询管理的组
    @Select("select checkgroup_id from t_checkgroup_checkitem where checkitem_id=#{id}")
    List<Integer> findThirdByCheckItemId(@Param("id") Integer id);
}
