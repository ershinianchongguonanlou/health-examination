package com.itheima.dao;

import com.itheima.Do.SetmealDo;
import com.itheima.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderDao {
    public void add(Order order);

    public List<Order> findByCondition(Order order);

    /**
     * 根据预约id查询预约信息，包括体检人信息、套餐信息
     */
    public Map findById4Detail(Integer id);

    //所有预约套餐name
    List<Integer> findAllSetmealId();

    //查询套餐预约占比数据统计
    Integer findCountBySetmeal(Integer setmealId);

    //拿到总人数
    Integer findTotalCount(@Param("start") LocalDate start, @Param("end") LocalDate end, @Param("orderStatus") String orderStatus);

    //预约最高的前四个
    List<SetmealDo> findHotSetmealId();

//    Map<Integer,Integer> findHotSetmealId();
}
