package com.itheima.dao;

import com.itheima.pojo.Setmeal;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: SetmealDao
 * @Description: 套餐管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:12
 */
@Mapper
public interface SetmealDao {

    //查询所有
    @Select("select * from t_setmeal")
    List<Setmeal> findAll();

    //根据id查询
    @Select("select * from t_setmeal where id=#{id}")
    Setmeal findById(Integer id);

    //根据条件查询
    List<Setmeal> findByCondition(@Param("queryString") String queryString);

    //新增
    void insert(Setmeal setmeal);

    //新增第三方表
    void insertThird(@Param("sId") Integer sId, @Param("cgId") Integer cgId);

    //根据套餐id，拿组id
    @Select("select checkgroup_id from t_setmeal_checkgroup where setmeal_id=#{id}")
    List<Integer> findThirdBysId(Integer id);

    //根据套餐ids，批量删除套餐
    void deleteByIds(@Param("setmealIdList") List<Integer> setmealIdList);

    //删掉第三方关联表，根据套餐id
    void deleteOtherThirdTableByIds(@Param("ids") List<Integer> ids);

    //先找关联套餐,根据组id，查询关联套餐id
    @Select("select setmeal_id from t_setmeal_checkgroup where checkgroup_id=#{checkGroupId}")
    List<Integer> findThirdByCheckGroupId(@Param("checkGroupId") Integer checkGroupId);
}
