package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Member;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface MemberDao {
    public List<Member> findAll();

    public Page<Member> selectByCondition(String queryString);

    public void add(Member member);

    public void deleteById(Integer id);

    public Member findById(Integer id);

    public Member findByTelephone(String telephone);

    public void edit(Member member);

    Integer findCountByRegTime(@Param("start") LocalDate start, @Param("end") LocalDate end);
}
