package com.itheima.controller;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckGroupController
 * @Description: 检查组管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:09
 */
@Api(tags = "CheckGroupController-检查组管理")
@RestController
@RequestMapping("/checkgroup")
public class CheckGroupController {

    @Autowired
    private CheckGroupService checkGroupService;

    //查询所有
    @ApiOperation(value = "查询所有", notes = "查询所有")
    @GetMapping("/findAll")
    public Result findAll() {
        List<CheckGroup> checkGroupList = checkGroupService.findAll();
        return new Result(true, "success", checkGroupList);
    }

    //根据id查询
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @GetMapping("/findById")
    public Result findById(Integer id) {
        CheckGroup checkGroup = checkGroupService.findById(id);
        return new Result(true, "success", checkGroup);
    }

    //根据组id查询对应的项
    @ApiOperation(value = "根据组id查询对应的项", notes = "根据组id查询对应的项")
    @GetMapping("/findCheckItemIdsByCheckGroupId")
    public Result findCheckItemIdsByCheckGroupId(@RequestParam("checkgroupId") Integer id) {
        List<Integer> checkItemIdList = checkGroupService.findCheckItemIdsByCheckGroupId(id);
        return new Result(true, "success", checkItemIdList);
    }

    //分页查询
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = checkGroupService.findPage(queryPageBean);
        //return new Result(true, "success", pageResult);
        return pageResult;
    }

    //修改
    @ApiOperation(value = "修改", notes = "修改")
    @PutMapping("/edit")
    public Result edit(@RequestBody CheckGroup checkGroup, @RequestParam List<Integer> checkitemIds) {
        checkGroupService.edit(checkGroup, checkitemIds);
        return new Result(true, "success");
    }

    //新增
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    public Result add(@RequestBody CheckGroup checkGroup, @RequestParam List<Integer> checkitemIds) {
        checkGroupService.add(checkGroup, checkitemIds);
        return new Result(true, "success");
    }

    //根据ID删除
    @ApiOperation(value = "根据ID删除", notes = "根据ID删除")
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById(@PathVariable Integer id) {
        try {
            checkGroupService.deleteById(id);
            return new Result(true, "success");
        } catch (Exception e) {
            return new Result(false, "删除失败");
        }
    }

}
