package com.itheima.controller;

import com.itheima.Vo.MemberRegVo;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.service.MemberService;
import com.itheima.service.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报表
 */
@Api(tags = "ReportController-报表相关接口")
@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private MemberService memberService;

    @ApiOperation(value = "会员统计", notes = "会员统计")
    @GetMapping("/getMemberReport")
    //会员统计
    public Result getMemberReport() {
        try {
            //首先在t_member表中
            // 获取年月时间列表
            //String[] dateTable = memberService.getDateTable();
            List<LocalDate> dateTable = memberService.getDateTable();
            //[2022-09-13, 2022-10-13, 2022-11-13, 2022-12-13, 2023-01-13, 2023-02-13,
            // 2023-03-13, 2023-04-13, 2023-05-13, 2023-06-13, 2023-07-13, 2023-08-13]
            // 获取会员总数
            //封装成一个对象 MemberRegVo
            List<MemberRegVo> memberRegVoList = memberService.getRegTime(dateTable);
            //封装为map集合<String，List>
            Map data = memberService.transformToMap(memberRegVoList);
            // 返回结果 months  memberCount
            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, data);
        } catch (Exception e) {
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL, null);
        }
    }

    //查询套餐预约占比数据统计
    @ApiOperation(value = "查询套餐预约占比数据统计", notes = "查询套餐预约占比数据统计")
    @GetMapping("/getSetmealReport")
    public Result getSetmealReport() {

        //调用Service查询
        Map<String, Double> setmealNamesAndSetmealCount = reportService.getSetmealNamesAndSetmealCount();

        //结果处理后封装到map集合
        //Map map = new HashMap();
        //map.put("setmealNames",setmealNames);
        // 返回结果:setmealCount  setmealCount
        //封装为map集合<String，List>
        Map map = reportService.setmealReportTransformToMap(setmealNamesAndSetmealCount);

        //  "阳光爸妈升级肿瘤12项筛查（男女单人）体检套餐": 0.2,
        //    "通用套餐": 0.1,
        //    "入职无忧体检套餐（男女通用）": 0.1,
        //    "珍爱高端升级肿瘤12项筛查（男女单人）": 0.2,
        //    "粉红珍爱(女)升级TM12项筛查体检套餐": 0.4
        return new Result(true, MessageConstant.GET_SETMEAL_COUNT_REPORT_SUCCESS, map);
    }

    @Autowired
    private ReportService reportService;

    //获取运营数据统计
    @ApiOperation(value = "获取运营数据统计", notes = "获取运营数据统计")
    @GetMapping("/getBusinessReportData")
    public Result getBusinessReportData() {

//        List<Object> operationalData = new ArrayList<>();
//
//        //获得当日日期
//        String localDateNow = reportService.getLocalDateNow();
//        operationalData.add(localDateNow);
//
//        //会员数据统计
//        MemberCountBo memberCountBo = reportService.getMemberCount();
//        operationalData.add(memberCountBo);
//
//        //预约到诊数据统计
//        OrderCountBo orderCountBo = reportService.getOrderCount();
//        operationalData.add(orderCountBo);
//
//        //热门套餐,封装
//        List<HotSetmealBo> hotSetmealList = reportService.getHotSetmeal();
//        operationalData.add(hotSetmealList);

        //封装成map
        try {
            Map<String, Object> businessReport = reportService.getBusinessReport();
            //获取运营数据统计，封装为map
            Map map = reportService.businessReportDataTransformToMap(businessReport);
            return new Result(true, MessageConstant.GET_BUSINESS_REPORT_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true, MessageConstant.GET_BUSINESS_REPORT_FAIL);
        }

    }

    //运营数据报表导出EXCEL
    @ApiOperation(value = "运营数据报表导出EXCEL", notes = "运营数据报表导出EXCEL")
    @GetMapping("/exportBusinessReport")
    public Result exportBusinessReport(HttpServletRequest request, HttpServletResponse response) {

        try {
            //下载为EXCEL
            reportService.exportExcel(request, response);
            return new Result(true, "导出Excel成功");
        } catch (Exception e) {
            return new Result(false, "导出Excel失败");
        }
    }

    //运营数据报表导出为PDF
    @ApiOperation(value = "运营数据报表导出为PDF", notes = "运营数据报表导出为PDF")
    @GetMapping("/exportBusinessReportPDF")
    public Result exportBusinessReportPDF(HttpServletRequest request, HttpServletResponse response) {

        try {
            //下载为PDF
            reportService.exportPDF(request, response);
            return new Result(true, "导出PDF成功");
        } catch (Exception e) {
            return new Result(false, "导出PDF失败");
        }
    }

}
