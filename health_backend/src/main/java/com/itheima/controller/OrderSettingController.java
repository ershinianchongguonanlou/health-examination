package com.itheima.controller;

import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.POIUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 预约设置操作
 */
@Api(tags = "OrderSettingController-预约设置管理")
@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {
    @Autowired
    private OrderSettingService orderSettingService;

    @ApiOperation(value = "预约设置导入Excel", notes = "预约设置导入Excel")
    @ApiParam(name = "excelFile", value = "Excel文件对象", required = true, type = "File")
    @PostMapping(value = "/upload")
    public Result upload(@RequestPart @RequestParam("excelFile") MultipartFile excelFile) {
        try {
            List<String[]> data = POIUtils.readExcel(excelFile);
            if (data != null && data.size() > 0) {
                List<OrderSetting> list = new ArrayList<>();
                for (String[] rowData : data) {
                    String date = rowData[0];
                    String number = rowData[1];
                    OrderSetting orderSetting =
                            new OrderSetting(new Date(date),
                                    Integer.parseInt(number));
                    list.add(orderSetting);
                }
                orderSettingService.add(list);
            }

            System.out.println(data);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
        return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);
    }

    @ApiOperation(value = "根据日期查询预约设置", notes = "根据日期查询预约设置")
    @ApiImplicitParam(name = "date", value = "日期", example = "yyyy-MM", required = true, dataType = "String")
    @GetMapping("/getOrderSettingByMonth")
    public Result getOrderSettingByMonth(String date) {//yyyy-MM
        try {
            List<Map> list = orderSettingService.getOrderSettingByMonth(date);
            //获取预约设置数据成功
            return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            //获取预约设置数据失败
            return new Result(false, MessageConstant.GET_ORDERSETTING_FAIL);
        }
    }

    @ApiOperation(value = "根据日期修改预约设置", notes = "根据日期修改预约设置")
    @ApiImplicitParam(name = "orderSetting", value = "订单设置对象", example = "yyyy-MM", required = true, dataType = "OrderSetting")
    @PutMapping("/editNumberByOrderDate")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting) {
        try {
            orderSettingService.editNumberByDate(orderSetting);
            //预约设置成功
            return new Result(true, MessageConstant.ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            //预约设置失败
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }
    }
}
