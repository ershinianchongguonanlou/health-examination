package com.itheima.controller;

import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetmealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: SetmealController
 * @Description: 套餐管理
 * @Author: HeKuo
 * @Date: 2023/8/1017:11
 */
@Api(tags = "SetmealController-套餐管理")
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    //查询所有
    @ApiOperation(value = "查询所有", notes = "查询所有")
    @GetMapping("/getSetmeal")
    public Result getSetmeal() {
        List<Setmeal> setmealList = setmealService.findAll();
        return new Result(true, "success", setmealList);
    }

    //根据id查询
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @GetMapping("/findById")
    public Result findById(Integer id) {
        Setmeal setmeal = setmealService.findById(id);
        return new Result(true, "success", setmeal);
    }

    //分页查询
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = setmealService.findPage(queryPageBean);
        return pageResult;
    }

    //新增
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    public Result add(@RequestBody Setmeal setmeal) {
        try {
            setmealService.add(setmeal);
            return new Result(true, MessageConstant.ADD_SETMEAL_SUCCESS);
        } catch (Exception e) {
            return new Result(false, MessageConstant.ADD_SETMEAL_FAIL);
        }
    }

    //根据ID删除
    @ApiOperation(value = "根据ID删除", notes = "根据ID删除")
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById(@PathVariable Integer id) {
        try {
            setmealService.deleteById(id);
            return new Result(true, "success");
        } catch (Exception e) {
            return new Result(false, "删除失败");
        }
    }

}
