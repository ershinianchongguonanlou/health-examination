package com.itheima.controller;

import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Order;
import com.itheima.service.OrderService;
import com.itheima.utils.SMSUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 体检预约
 */
@Api(tags = "OrderController-体检预约管理")
@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private RedisTemplate redisTemplate;

    @ApiOperation(value = "体检预约", notes = "体检预约")
    @ApiImplicitParam(name = "map", value = "体检预约相关参数", required = true, dataType = "Map")
    @PostMapping("/submit")
    public Result submit(@RequestBody Map map) {
        String telephone = (String) map.get("telephone");
        String validateCode = (String) map.get("validateCode");
        //从redis中获取保存的验证码

        String codeInRedis = (String) redisTemplate.opsForValue().get(telephone + RedisConstant.SENDTYPE_ORDER);
        if (codeInRedis == null || validateCode == null || !validateCode.equals(codeInRedis)) {
            //验证码校验失败
            return new Result(false, MessageConstant.VALIDATECODE_ERROR);
        }
        Result result = null;
        //验证码校验通过
        try {
            map.put("orderType", Order.ORDERTYPE_WEIXIN);
            result = orderService.order(map);
        } catch (Exception e) {
            e.printStackTrace();
            //预约失败
            result = new Result(false, MessageConstant.ORDER_FAIL);
        }

        if (result.isFlag()) {
            //预约成功，发送短信提示
            //预约成功，发送短信通知
            String orderDate = (String) map.get("orderDate");
            try {
                SMSUtils.sendShortMessage(SMSUtils.ORDER_NOTICE, telephone, orderDate);
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @ApiOperation(value = "根据ID查询体检预约", notes = "根据ID查询体检预约")
    @ApiImplicitParam(name = "id", value = "预约主键", required = true, dataType = "Integer")
    @GetMapping("/findById")
    public Result findById(Integer id) {
        try {
            Map map = orderService.findById(id);
            //查询预约信息成功
            return new Result(true, MessageConstant.QUERY_ORDER_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            //查询预约信息失败
            return new Result(false, MessageConstant.QUERY_ORDER_FAIL);
        }
    }
}
