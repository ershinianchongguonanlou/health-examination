package com.itheima.controller;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ProjectName: project-health
 * @Title: CheckItemController
 * @Description: 检查项管理
 * @Author: HeKuo
 * @Date: 2023/8/1016:59
 */
@Api(tags = "CheckItemController-检查项管理")
@RestController
@RequestMapping("/checkitem")
public class CheckItemController {
    @Autowired
    private CheckItemService checkItemService;

    //查询所有
    @ApiOperation(value = "查询所有", notes = "查询所有")
    @GetMapping("/findAll")
    public Result findAll() {
        List<CheckItem> checkItemList = checkItemService.findAll();
        return new Result(true, "success", checkItemList);
    }

    //根据id查询
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @GetMapping("/findById")
    public Result findById(Integer id) {
        CheckItem checkItem = checkItemService.findById(id);
        return new Result(true, "success", checkItem);
    }

    //分页查询
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = checkItemService.findPage(queryPageBean);
        //return new Result(true, "success", pageResult);
        return pageResult;
    }

    //修改
    @ApiOperation(value = "修改", notes = "修改")
    @PutMapping("/edit")
    public Result edit(@RequestBody CheckItem checkItem) {
        checkItemService.edit(checkItem);
        return new Result(true, "success");
    }

    //根据ID删除
    @ApiOperation(value = "根据ID删除", notes = "根据ID删除")
    @DeleteMapping("/deleteById")
    public Result deleteById(Integer id) {
        checkItemService.deleteById(id);
        return new Result(true, "success");
    }

    //新增
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    public Result add(@RequestBody CheckItem checkItem) {
        checkItemService.add(checkItem);
        return new Result(true, "success");
    }
}
